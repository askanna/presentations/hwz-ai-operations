---
# Created with Marp: https://marpit.marp.app/
marp: true
theme: uncover
class: invert
backgroundColor: #5d3eb2
backgroundImage: url(https://askanna.io/assets/img/hero.jpg)
---

<style>
a {
  /* Override styling */
  color: #faed7a !important;
}
</style>

<a href="https://askanna.io" target="_blank" style="color:#faed7a;">

![h:300](images/askanna-logo.svg)

# Collaborate on Data Science

</a>

<!--
Track what you do
Compare results
Share it with "everyone"

Make it simple to:
- use cloud computing
- setup APIs
- integrate with other apps

Playground: just try it
Save time -> more real collaboration

Reproduce results
-->

---

# How can we support <span style="color:#faed7a;">transparency</span>?

<!--
Hot topic. Governance. EU regulation
No standardization yet!

Many components. More than explainable AI.
-->

---

# [Click to open<br>the AskAnna demo](https://beta.askanna.eu/6swz-ujcr-jQQw-SAdZ)

<!--
Show core concept of AskAnna in a demo project

- Project
- Code
- Config in askanna.yml 
- Jobs
- Runs
-->

---

# Develop a model

![](images/data-science-flow-for-develop.png)

<!--
It all starts with a question

Get your data

Train models...and work on your data

Does it answer the question?
Yes! Let's deploy the model

Continuous monitoring
Also continuous validate objective

Next, operationalize
-->

---

# Daily prediction

![](images/daily-prediction.png)

<!--
Prediction use trained model as input

Data scientist needed for support
and continuous improvement

Who is accountable? How can I be in control?
AskAnna is a tool to support you.
-->

---

## To be in control & transparant,</br>you need to know:

<style scoped>
td, th {
  /* Override contextual styling */
  border: 0 !important;
}
</style>

| | |
| :--- | :--- |
| data used | code version |
| models applied &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | model configuration |
| variables set | metrics |
| results | artifacts |
| run environment | (error) logs |

<!--
More complex, now we start using pretrained "open source" models
-->

---

# Let's show more

<!--

Explain projects

Show job schedules to gather data
  Monitoring: raise failure and receive notifications

Show pandas profiling report to explore data

Show select best model, and use it in serve job

-->

---

![h:600](images/demo-1.png)

---

![h:500](images/demo-2.png)

---

![h:400](images/demo-3.png)

---

![h:600](images/demo-4.png)

---

# With AskAnna we help to

### <span style="color:#faed7a;">reproduce</span> results

### <span style="color:#faed7a;">explain</span> what's done

### be <span style="color:#faed7a;">in control</span>

---

# Try AskAnna<br>[beta.askanna.eu](htts://beta.askanna.eu)

[robbert@askanna.io](mailto:robbert@askanna.io) | [LinkedIn](https://www.linkedin.com/in/robbertbos/)
